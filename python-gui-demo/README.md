# Python-GUI-Demo

> 基于Python 3.5 + PyQt 5的窗体Demo项目

开发部署步骤

## 1. 安装Anacoda或其他Python 3.5+发行版本

## 2. 开发GUI
以Anaconda for Windows为例，打开以下程序，即为设计器：
```
C:\Program Files\Anaconda3\Library\bin\designer.exe
```
创建一个MainWindows，并在上面放置一个label和一个pushButton

## 3. 生成代码
以Anaconda for Windows为例，用文本编辑器**修改**以下脚本：
```
C:\Program Files\Anaconda3\Library\bin\pyuic5.bat
```
设置好Python的路径，例如修改为以下内容：
```bat
@python.exe -m PyQt5.uic.pyuic %1 %2 %3 %4 %5 %6 %7 %8 %9
```
然后在终端下运行以下命令，将`*.ui`文件转换为`*.py`文件。
```cmd
 pyuic5 MainWindow.ui -o MainWindow.py
```

## 4. 添加事件
在生成的`*.py`文件中，添加事件相关代码，例如：
```python
import sys
...
self.pushButton.clicked.connect(lambda: onClick(self, MainWindow))
...
def onClick(self, MainWindow):
    _translate = QtCore.QCoreApplication.translate
    sender = MainWindow.sender()
    self.label.setText(_translate("MainWindow", sender.text()))
    MainWindow.statusBar().showMessage(_translate("MainWindow", sender.text() + ' was pressed'))

def main():    
    app = QtWidgets.QApplication(sys.argv)
    window = QtWidgets.QMainWindow()
    Ui_MainWindow().setupUi(window)
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
```