# WEBP图片格式转换

`convert_webp.py`为将PNG图片转换为WEBP格式的脚本。

WEBP为Google开发的图片格式，压缩程序高，加载速度快。

可能出现的问题：

* 报错，内容大致为无法转WEBP格式

```bash
pip install Pillow
```

* PNG文件放到`img`文件夹，并为WEBP输出创建`img_webp`文件夹
