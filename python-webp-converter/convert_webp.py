# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 17:07:27 2016

@author: Jun
"""

import glob
import os
import threading

from PIL import Image


def create_image(infile, index):
    os.path.splitext(infile)
    im = Image.open(infile)
    im.save("img_webp/"+os.path.splitext(os.path.basename(infile))[0]+ ".webp", "WEBP")


def start():
    index = 0
    for infile in glob.glob("img/*.png"):
        t = threading.Thread(target=create_image, args=(infile, index,))
        t.start()
        t.join()
        index += 1


if __name__ == "__main__":
    start()